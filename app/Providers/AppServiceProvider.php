<?php

namespace App\Providers;

use App\Console\ImportDelimitedCommand;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $commands = collect([
            'command.artisancsv.import-delimited' => ImportDelimitedCommand::class,
        ]);

        $commands->each(function ($command, $abstract) {
            $this->app->singleton($abstract, function () use ($command) {
                return new $command();
            });
        });

        $this->commands($commands->keys()->toArray());
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
